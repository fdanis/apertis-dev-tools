=head1 NAME

devroot-enter - Create a namespace container in the specified devroot

=head1 SYNOPSIS

B<devroot-enter> I<DEVROOT> [I<OPTIONS>...] [I<COMMAND> [I<ARGS>...]]

=head1 DESCRIPTION

B<devroot-enter> is a wrapper for B<systemd-nspawn> which sets up a namespace
container in a devroot of user's choice. Devroot is file system hierarchy
based on a build of the Apertis system. Devroot is a part of the SDK image
and is being created at the time of the build from the same binaries as the
target image, but with additional tools pre-installed, such as native compilers,
debuggers and other development tools. Devroot is currently using binaries for
I<armhf> architecture.

Devroot can be used to natively compile or debug binaries for the target system,
build binary packages etc. Using B<devroot-enter> is preferrable to using
B<chroot>, since B<devroot-enter> fully virtualizes the file system hierarchy,
as well as the process tree, the various IPC subsystems and the host and
domain name, while B<chroot> does none of that and also requires the user to
manually bind-mount directories such as F</dev> and F</proc> from the host
system.

B<devroot-enter> makes host's F</etc/resolv.conf> and statically-compiled
Arm qemu (B<qemu-arm-static>) available inside the container. A world-writable
tmpfs is mounted as F</tmp>. The initial current directory is set to F</tmp>.
The user's home directory is mounted r/w at the same path as outside the devroot.

=head1 OPTIONS

A summary of options is included below.

=over

=item I<DEVROOT>

Root directory of the devroot. This directory will be used as file system root for the container.

=item I<OPTIONS>

Additional options to pass to B<systemd-nspawn>, S<e. g.>:

=over

=item B<--bind=>I<PATH[:PATH[:OPTIONS]]>, B<--bind-ro=>I<PATH[:PATH[:OPTIONS]]>

Bind mount a file or directory from the host into the container.
The B<--bind-ro=> option creates read-only bind mounts.

=item B<--overlay=>I<PATH[:PATH...]:PATH>, B<--overlay-ro=>I<PATH[:PATH...]:PATH>

Combine multiple directory trees into one overlay file system and mount it into the container.
If B<--overlay-ro=> is used instead of B<--overlay=>, a read-only overlay file system is created.

=back

=item I<COMMAND>

Command to run in the container; shell is the default

=item I<ARGS>

Optional arguments to the command

=back

=head1 ENVIRONMENT

=over

=item I<LD_PRELOAD>

Since the binary architecture inside the devroot is different, B<devroot-enter> unsets this
environment variable to prevent warnings from being displayed.

=back

=head1 SEE ALSO

L<systemd-nspawn(1)>, L<qemu-user-static(1)>

=head1 COPYRIGHT

Copyright (C) 2018, Collabora Ltd

=head1 AUTHOR

Andrej Shadura L<< <andrewsh@collabora.com> >>
