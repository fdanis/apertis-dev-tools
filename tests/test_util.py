# -*- coding: utf-8 -*-
#
# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

import base64
import http.server
import os
import socket
import subprocess
import threading
import tempfile
import shutil
import atexit
from pathlib import Path
from contextlib import contextmanager

BASE_TAG = "{0}-{1}-{2}_{3}"
BASE_ARCHIVE = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'files', 'archives', "{0}.tar.gz".format(BASE_TAG))
BASE_CONFIG  = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'files', 'configs', "{0}.conf".format(BASE_TAG))
BASE_INSTALL  = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'files', 'install')
BASE_MOCK  = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'mock')

FERMIS_USER = "fermis"
FERMIS_PASSWORD = "secret"
FERMIS_KEY = base64.b64encode("{0}:{1}".format(FERMIS_USER, FERMIS_PASSWORD).encode('ascii'))

# All available sysroot versions
SYSROOTS = [
    ('apertis',   '16.09', 'armhf', '20161031.0'),
    ('apertis',   '16.09', 'armhf', '20161031.1'),
    ('apertis',   '16.09', 'armhf', '20161101.0'),
    ('apertis',   '16.09', 'armhf', '20161101.2'),
    ('apertis',   '16.09', 'armhf', '20161101.10'),
    ('apertis',   '16.09', 'arm64', '20161101.0'),
    ('apertis',   '16.12', 'armhf', '20161101.0'),
    ('apertis',   'v2019dev0', 'armhf', '20190401.0'),
    ('apertis',   'v2019dev0', 'armhf', '20190404.0'),
    ('apertis',   'v2019pre',  'armhf', '20190629.0'),
    ('apertis',   'v2019.0',   'armhf', '20190921.0'),
    ('apertis',   'v2019.1',   'armhf', '20191216.0'),
    ('babiladis', '16.12', 'armhf', '20161101.0'),
    ('fermis',    '16.12', 'armhf', '20161101.0'),
    ('icarus',    'wings', 'armhf', '20170316.0')
]

# Only latest build (YYYYMMDD.BB) of each sysroot (distro/release/arch)
LATEST_SYSROOTS = [
    ('apertis',   '16.09', 'armhf', '20161101.10'),
    ('apertis',   '16.09', 'arm64', '20161101.0'),
    ('apertis',   '16.12', 'armhf', '20161101.0'),
    ('apertis',   'v2019dev0', 'armhf', '20190404.0'),
    ('apertis',   'v2019pre',  'armhf', '20190629.0'),
    ('apertis',   'v2019.0',   'armhf', '20190921.0'),
    ('apertis',   'v2019.1',   'armhf', '20191216.0'),
    ('babiladis', '16.12', 'armhf', '20161101.0'),
    ('fermis',    '16.12', 'armhf', '20161101.0')
]

# Sysroots that need authentication
PROTECTED_SYSROOTS = [
    ('fermis',    '16.12', 'armhf', '20161101.0')
]

# Incrementing version to test updates
SYSROOT1 = ('apertis', '16.09', 'armhf', '20161031.0')  #First
SYSROOT2 = ('apertis', '16.09', 'armhf', '20161101.10') #Last
SYSROOT_VERSIONS = [
    ('apertis',   '16.09', 'armhf', '20161031.0'),
    ('apertis',   '16.09', 'armhf', '20161031.1'),
    ('apertis',   '16.09', 'armhf', '20161101.0'),
    ('apertis',   '16.09', 'armhf', '20161101.2'),
    ('apertis',   '16.09', 'armhf', '20161101.10')
]

# Invalid sysroots
BAD_SYSROOTS = [
    ('eraroj',    '16.09', 'armhf', 'missing_file'),
    ('eraroj',    '16.09', 'armhf', 'invalidarchive'),
    ('eraroj',    '16.06', 'armhf', 'invalidversion'),
    ('eraroj',    '16.09', 'x86',   'invalidarchitecture')
]

CONFIG_FILES = [
    "latest.conf",
    "sections.conf"
]

BAD_VERSION_URLS = [
    'invalidformat',
    'http://HOST/not_found',
    'http://HOST/versions/empty',
    'http://HOST/fermis/unauthorized',
    'http://HOST/versions/empty',
    'http://HOST/versions/missing_version',
    'http://HOST/versions/missing_url',
    'http://HOST/versions/invalid_version'
]

BAD_SYSROOT_URLS = [
    'http://HOST/versions/wrong_distro',
    'http://HOST/versions/wrong_release',
    'http://HOST/versions/invalid_archive',
    'http://HOST/versions/invalid_sysroot'
]

BAD_URLS = BAD_VERSION_URLS + BAD_SYSROOT_URLS

def run_cmd(*args):
    executable = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'tools', 'ade')
    cmd = [executable, '--format', 'parseable', *args]
    return subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

def add_auth_params(params, sysroot):
    if sysroot in PROTECTED_SYSROOTS:
        params += ['--user', FERMIS_USER, '--password', FERMIS_PASSWORD]

def should_succeed(*args, check=None):
    result = run_cmd(*args)
    if result.returncode:
        print("Command '{0}' should have succeeded".format(" ".join(result.args)))
        print(result.stderr.decode('utf-8'))
        exit(1)
    output = dict()
    for line in result.stdout.decode('utf-8').strip().split('\n'):
        if ':' in line:
            key, value = line.split(':', 1)
            output[key] = value
    if check and not check(output):
        exit(1)
    return output

def should_fail(*args, check=None):
    result = run_cmd(*args)
    if not result.returncode:
        print("Command '{0}' should have failed".format(" ".join(result.args)))
        exit(1)
    output = result.stderr.decode('utf-8').strip()
    if check and not check(output):
        exit(1)
    return output

def split_elements(string):
    if not string:
        return []
    return string.split(';')

# Setup HTTP server
class SysrootHTTPRequestHandler(http.server.SimpleHTTPRequestHandler):
    def needs_authentication(self):
        return 'fermis' in self.path

    def do_AUTHHEAD(self):
        self.send_response(401)
        self.send_header('WWW-Authenticate', 'Basic realm=\"Test\"')
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self, *args):
        if self.needs_authentication():
            key = self.headers.get('Authorization')
            if not key or key.encode('ascii') != b'Basic ' + FERMIS_KEY:
                self.do_AUTHHEAD()
                pass
        super().do_GET(*args)

    def log_message(self, *args):
        pass

def template_config_file(server, src, dst):
    with open(src) as fsrc:
        host = "{}:{}".format(*server.server_address)
        output = []
        for line in fsrc.readlines():
            output.append((line.replace("HOST", host)))

    with open(dst, 'w') as fdst:
        fdst.writelines(output)

class SysrootServer(http.server.HTTPServer):
    def __init__(self):
        contentdir = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                  'files')
        tmpdir = tempfile.TemporaryDirectory()
        atexit.register (tmpdir.cleanup)

        self.tmpcontentdir = os.path.join(tmpdir.name, 'files')
        shutil.copytree(contentdir, self.tmpcontentdir)
        os.chdir(self.tmpcontentdir)

        # This binds so we can determine the port that's used
        super().__init__(("127.0.0.1", 0), SysrootHTTPRequestHandler)

        for f in Path(self.tmpcontentdir).rglob("versions/*"):
            template_config_file (self, f.as_posix(), f.as_posix())

        for f in Path(self.tmpcontentdir).rglob("configs/*"):
            template_config_file (self, f.as_posix(), f.as_posix())

    def server_bind(self):
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        super().server_bind()

    def start(self):
        self.thread = threading.Thread(target=self.serve_forever)
        self.thread.daemon = True
        self.thread.start()

    def stop(self):
        self.shutdown()
        self.thread.join()

    def host(self):
        return "{}:{}".format(*self.server_address)

    def base_url(self):
        url = "http://{}/versions/{}"
        return url.format(self.host(), BASE_TAG)

    def base_archive_url(self):
        url = "http://{}/archives/{}.tar.gz"
        return url.format(self.host(), BASE_TAG)

@contextmanager
def templatedconfig(server, config_file):
    config = os.path.join(os.getcwd(), "configs", config_file)
    with tempfile.TemporaryDirectory() as t:
        tmpconfig = os.path.join(t, os.path.basename(config_file))

        template_config_file(server, config, tmpconfig)
        yield tmpconfig
